import {Injectable, Inject} from "@angular/core";
import {Http, Headers, RequestOptions} from "@angular/http";
import {Observable} from "rxjs/Observable";
import "rxjs/Rx";

@Injectable()
export class SpotifyService {
    static BASE_URL = "https://api.spotify.com/v1";
    static TOKEN = "BQCm6C9iZfRVxdRwrQYAeVS6sVgVklwUHo41_TlvA6MQYq8mQZfNTXug9xDSAL1MAfmzGMkmTcPYXK_Ddp4mozsysRxBj1m6VuVG4uQBhGJjkODHh2_bMc08N_k4JZ87fWP0i3_HGwKH7k5297aIJekFT6bJBaI";

    constructor(private http: Http) {

    }

    query(URL: string, params?: Array<string>): Observable<any[]> {
        let queryUrl = `${SpotifyService.BASE_URL}${URL}`;
        if (params) {
            queryUrl = `${queryUrl}?${params.join('&')}`;
        }

        let headers = new Headers();
        headers.append("Authorization", `Bearer ${SpotifyService.TOKEN}`);
        let opts = new RequestOptions();
        opts.headers = headers;
        return this.http.request(queryUrl, opts).map((res: any) => res.json());
    }

    search(query: string, type: string): Observable<any[]> {
        return this.query(`/search`, [`q=${query}`, `type=${type}`]);
    }
    searchTrack(query: string): Observable<any> {
        return this.search(query, 'track');
    }

    getTrack(id: string): Observable<any[]> {
        return this.query(`/tracks/${id}`);
    }
    getArtist(id: string): Observable<any[]> {
        return this.query(`/artists/${id}`);
    }
    getAlbum(id: string): Observable<any[]> {
        return this.query(`/albums/${id}`);
    }
}

export const SPOTIFY_PROVIDERS: Array<any> = [
    {provide: SpotifyService, useClass: SpotifyService}
];
